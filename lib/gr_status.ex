defmodule GrStatus do
  use GenServer

  require Logger
  require HTTPoison

  @moduledoc """
  Documentation for `GrStatus`.
  Call `start_link/0` to start polling every 30s.
  """

  def init(_) do
    Logger.debug("Setting up a polling timer for 30s interval")
    :timer.send_interval(30_000, :poll)
    {:ok, %{}}
  end

  def start_link do
    GenServer.start_link(__MODULE__, nil)
  end

  def handle_info(:poll, state) do
    start_time = :os.system_time(:millisecond)

    url = "https://apis.globalreader.eu/health"
    options = [ssl: [{:versions, [:"tlsv1.2"]}], recv_timeout: 400]
    response = HTTPoison.get(url, [], options)

    end_time = :os.system_time(:millisecond)

    status_msg =
      case response do
        {:ok, resp} ->
          "Status: #{resp.status_code}, #{resp.body}. Took #{end_time - start_time}ms"
        {:error, resp} ->
          "Status: ERROR, #{resp.reason}. Took #{end_time - start_time}ms"
      end

    Logger.info(status_msg)

    {:noreply, state}
  end
end
